# programmed by Franco Mele on jun 8th, 2021
#
#
#
# programma per l'estrazione di eventi e parametri di qualità da server web (in xml);
# produce il file di input per una procedura che plotta una mappa con le qualità
# delle localizzazioni.  
# I criteri per definire le qualità sono 
# (vedere Amato - Mele 2008 Performance of the INGV National Seismic Network from 1997 to 2007)

# The first quality rating is based on errors and goodness-of-fit: 
# Score
#  1.5   A. RMS < 0.45 sec  and  ERH <2.0 km  and  ERZ <4.0 km
#  0.5   B. RMS < 0.90 sec  and  ERH <5.0 km  and  ERZ <10.0 km
# -0.5   C. RMS < 1.50 sec  and  ERH <10.0 km 
# -1.5   D. Worse than above 
#
# The second quality rating is based on station geometry: 
# Score
#  3.0   A. NWR > 6  and  MAXGAP < 90  and  either  DMIN < DEPTH  or  DMIN <10.0 
#  1.0   B. NWR > 6  and  MAXGAP < 135  and  either  DMIN < 2*DEPTH  or  DMIN <20 
# -1.0   C. NWR > 6  and  MAXGAP < 180  and  DMIN <100 
# -3.0   D. Worse than above 

# La somma dei due valori corrispondenti alle due qualità può assumere 
# 10 valori (-4.5, -3.5, ..-0.5, 0.5, ..., 4.5)
#
#  l'uscita del programma è un file di input per GMT contenete 5 colonne:
#  longitude , latitude , score_di_qualità , dimensione(M*M/40) , codice-formato(funzione della depth)
#
#  14.0678 43.0577 -2.5    0.24649 n
#  14.3110 38.5290 -2.5    0.18225 n
#  11.1080 46.7760 -3.5    0.24806 a
#  10.2478 44.6922 -1.5    0.15876 s
#  15.2043 38.3988 -1.5    0.20449 a
#  15.7208 39.2178  1.5    0.28730 a
#  15.7802 39.5208 -1.5    0.20880 a
#  14.2312 37.8287  3.5    0.16002 s
#
# i codici GMT sono t, s, n, h, g, a (per i limiti di depth: 0 -> 5, 10, 30, 60, 100, > 100)



# CRITERI ORIGINALI di Hypoinverse2000:
#
# The first quality rating is based on errors and goodness-of-fit: 
# 
#     A. RMS < 0.15 sec  and  ERH <1.0 km  and  ERZ <2.0 km
#     B. RMS < 0.30 sec  and  ERH <2.5 km  and  ERZ <5.0 km
#     C. RMS < 0.50 sec  and  ERH <5.0 km 
#     D. Worse than above 
#
# The second quality rating is based on station geometry: 
# 
#     A. NWR > 6  and  MAXGAP < 90  and  either  DMIN < DEPTH  or  DMIN <5.0 
#     B. NWR > 6  and  MAXGAP < 135  and  either  DMIN < 2*DEPTH  or  DMIN <10 
#     C. NWR > 6  and  MAXGAP < 180  and  DMIN <100 
#     D. Worse than above 



import pandas as pd
import requests
import xmltodict
import math


def main():
#  http://terremoti.ingv.it/events?last_nd=-1&starttime=2021-05-08&endtime=2021-05-15&minmag=2&maxmag=10&wheretype=pointradius&box_search=Mondo&minlat=-90&maxlat=90&minlon=-180&maxlon=180&municipio=&lat=42&lon=13&maxradiuskm=30&mindepth=-10&maxdepth=1000

  typeq = input(" selezione rettangolare (1) o circolare (2) ? ")

  if( typeq == "1" ):
       minlat = input(" latitudine minima (ES: 34.0) ")
       maxlat = input(" latitudine massima (ES: 49.0) ")
       minlon = input(" longitudine minima (ES: 4.0) ")
       maxlon = input(" longitudine massima (ES: 23.0) ")
  elif( typeq =="2" ):
       lat = input(" latitudine del centro  ")
       lon = input(" longitudine del centro ")
       radius = input(" raggio del cerchio (km) ")
  else:
       print(" non hai scelto né 1 né 2 ")
       exit()

  mindepth = input(" profondita' minima (ES: 0) ")
  maxdepth = input(" profondita' massima (ES: 700.0) ")
  minmag = input(" magnitudo minima  (ES: 2.5) ")
  maxmag = input(" magnitudo massima (ES: 10.0) ")
  startt = input(" data di inizio nella forma yyyy-mm-dd ")
  endt   = input(" data di fine nella forma yyyy-mm-dd ")
  
  print(" estraggo i dati del periodo ",startt, " ", endt)


  if( typeq =="1" ):
     url = "http://webservices.ingv.it/fdsnws/event/1/query?minlat="+minlat+"&maxlat="+maxlat+ \
          "&minlon="+minlon+"&maxlon="+maxlon+ \
          "&mindepth="+mindepth+"&maxdepth="+maxdepth+ \
          "&starttime="+startt+"&endtime="+endt+ \
          "&minmag="+minmag+"&maxmag="+maxmag
  else:
     url = "http://webservices.ingv.it/fdsnws/event/1/query?wheretype=pointradius"+ \
           "&lat="+lat+ \
           "&lon="+lon+ \
           "&maxradiuskm="+radius+ \
           "&mindepth="+mindepth+"&maxdepth="+maxdepth+ \
           "&starttime="+startt+"&endtime="+endt+ \
           "&minmag="+minmag+"&maxmag="+maxmag
  url0 = url.replace(" ","")
  print(url0)
  response = requests.get(url0)
  with open('my_text.xml', 'wb') as fil:
      fil.write(response.content)

#opening the xml file in read mode
  with open("my_text.xml","r") as xml_obj:
    #coverting the xml data to Python dictionary
    my_dict = xmltodict.parse(xml_obj.read())
    #closing the file
    xml_obj.close()

#for val in my_dict.values():
#     print(type(val))

#for key in my_dict.keys():
#    print(key)

#for key in my_dict["q:quakeml"].keys():
#     print(key)

#for key in my_dict["q:quakeml"].keys():
#     print(key)

#  print(" --- START of keys in eventParameter --- ")
#  for key in my_dict["q:quakeml"]["eventParameters"].keys():
#     print(key)
#  print(" --- END of keys in eventParameter --- ")

#  print(" --- START of keys in event --- ")
#  for val  in my_dict["q:quakeml"]["eventParameters"]["event"]:
#      print(val)
#  print(" --- END of keys in event --- ")

#for val in my_dict["q:quakeml"]["eventParameters"].values():
#     print(val, "\n")




  filout = startt+"_"+endt+"_quality.txt"
  filo = filout.replace("-","")
  filo = filo.replace(" ","")
  filo = filo.replace(":","")


  with open(filo, 'w') as f:
    i=0
    for event in my_dict["q:quakeml"]["eventParameters"]["event"]:
      # print(event)
      print(event["description"]["text"])
      print(event["origin"]["time"])

      if(event["origin"]["creationInfo"].get("version")):
        version = event["origin"]["creationInfo"]["version"]
        print(" version: ", version)
        # ======================================================================
        if(version == "1000"):
                 # ....................=============================================
                 lat = float(event["origin"]["latitude"]["value"])
                 print(" lat. ",lat,"   cos(lat) :",math.cos(math.radians(lat)))

                 if(event["origin"]["latitude"].get("uncertainty")):
                     laterr=float(event["origin"]["latitude"]["uncertainty"]) * 111.19  # from deg to km
                     print ("lat. uncert: ", laterr)
                 else:
                     print("NULL")
                     laterr=0.0

                 lon = float(event["origin"]["longitude"]["value"])
                 print(" long. ", lon)

                 if(event["origin"]["longitude"].get("uncertainty")):
                      lonerr=float(event["origin"]["longitude"]["uncertainty"]) \
                      * 111.19 * math.cos(math.radians(lat))                            # from deg to km
                      print ( "long. uncert: ", lonerr)
                 else:
                      print("NULL")
                      lonerr=0.0

                 print("depth type: ",event["origin"]["depthType"])
                 if(event["origin"]["depth"].get("value")):
                      dep = float(event["origin"]["depth"]["value"]) /1000.0   # da metri a km
                      print("depth : ", dep)
                 else:
                      print("NULL")
                      dep = 10.0

                 if(event["origin"]["depth"].get("uncertainty")):
                      deperr = float(event["origin"]["depth"]["uncertainty"]) / 1000.0   # da metri a km
                      print ("depth uncert: ",  deperr)
                 else:
                      print("NULL")
                      deperr = 5.0

                 if(event["origin"].get("originUncertainty")):

                    if(event["origin"]["originUncertainty"].get("horizontalUncertainty")):
                        errh = float(event["origin"]["originUncertainty"]["horizontalUncertainty"]) / 1000.0 # da metri a km
                        print("horiz. uncert: ", errh)
                    else:
                        print("NULL")
                        errh = 5.0
                 else:
                    errh = 5.0


                 if(event["origin"]["quality"].get("usedStationCount")):
                      usedsta = float(event["origin"]["quality"]["usedStationCount"])
                      print(" used stations: ", usedsta)
                 else:
                      print("NULL")
                      usedsta = 3.

                 if(event["origin"]["quality"].get("usedPhaseCount")):
                      usedpha = float(event["origin"]["quality"]["usedPhaseCount"])
                      print(" used phases: ", usedpha)
                 else:
                      print("NULL")
                      usedpha = 3.

                 if(event["origin"]["quality"].get("azimuthalGap")):
                      azimgap = float(event["origin"]["quality"]["azimuthalGap"])
                      print(" azim gap: ", azimgap )
                 else:
                      print("NULL")
                      azimgap = 350.

                 if(event["origin"]["quality"].get("minimumDistance")):
                     mindist = float(event["origin"]["quality"]["minimumDistance"]) * 111.19  # from deg to km
                     print(" min dist: ", mindist)
                 else:
                     print("NULL")
                     mindist = 50.

                 if(event["origin"]["quality"].get("maximumDistance")):
                     maxdist = float(event["origin"]["quality"]["maximumDistance"]) * 111.19  # from deg to km
                     print(" max dist: ", maxdist)
                 else:
                     print("NULL")
                     maxdist = 1000.

                 if(event["origin"]["quality"].get("standardError")):
                     stderr = float(event["origin"]["quality"]["standardError"])
                     print(" standard error: ", stderr)
                 else:
                     print("NULL")
                     stderr = 3.0

                 if(event["origin"]["creationInfo"].get("version")):
                     version = event["origin"]["creationInfo"]["version"]
                     print(" version: ", version)
                 else:
                     print("NULL")
                     version = "000"


                 print(event["description"]["text"])

                 if( event.get("magnitude")  and  event["magnitude"]["mag"].get("value")):
                     mag = float(event["magnitude"]["mag"]["value"])
                     magtype = event["magnitude"]["type"]
                     print("magnitude: ", magtype," ",mag)
                 else:
                     print("NO MAG !")
                     mag = -9.9
                     magtype = "NOMAG"

# applico i CRITERI ORIGINALI di Hypoinverse2000, con gli scores di Amato-Mele (2008)
#
# The first quality rating is based on errors and goodness-of-fit: 
# 
#   1.5   A. RMS < 0.15 sec  and  ERH <1.0 km  and  ERZ <2.0 km
#   0.5   B. RMS < 0.30 sec  and  ERH <2.5 km  and  ERZ <5.0 km
#  -0.5   C. RMS < 0.50 sec  and  ERH <5.0 km 
#  -1.5   D. Worse than above 
#
# The second quality rating is based on station geometry: 
# 
#   3.0   A. NWR > 6  and  MAXGAP < 90  and  either  DMIN < DEPTH  or  DMIN <5.0 
#   1.0   B. NWR > 6  and  MAXGAP < 135  and  either  DMIN < 2*DEPTH  or  DMIN <10 
#  -1.0   C. NWR > 6  and  MAXGAP < 180  and  DMIN <100 
#  -3.0   D. Worse than above 
                 
                 # primo criterio:
                 if(    stderr < 0.15 and errh < 1.0 and deperr < 2.0 ):
                     score1 = 1.5
                 elif(  stderr < 0.30 and errh < 2.5 and deperr < 5.0 ):
                     score1 = 0.5
                 elif(  stderr < 0.50 and errh < 5.0 and deperr < 5.0 ):
                     score1 = -0.5
                 else:
                     score1 = -1.5

                 # secondo criterio:
                 # Non ho a disposizione i pesi delle fasi, perciò adotto un criterio
                 # diverso: il criterio ricchiederebbe che la somma dei pesi delle fasi usate sia 6.
                 # Supponendo un valore medio dei pesi attribuito alle fasi di 0.75, richiedo che il numero
                 # totale di fasi sia almeno 8.


                 if(     usedpha > 8   and   azimgap <  90.  and ( mindist < dep or mindist < 5.0)):
                       score2 = 3.0
                
                 elif(   usedpha > 8   and   azimgap < 135.  and ( mindist < 2*dep or mindist < 10.0)):
                       score2 = 1.0
                
                 elif(   usedpha > 8   and   azimgap < 180.  and mindist < 100.0):
                       score2 = -1.0
                
                 else:
                       score2 = -3.0


                 score = score1 + score2

                 print(" score1 score2  scoretot ", score1," ", score2," ", score)


# i codici GMT sono t, s, n, h, g, a (per i limiti di depth: 0 -> 5, 10, 30, 60, 100, > 100)
                 if (   dep <= 5):
                    dsig = "t"
                 elif(  dep <= 10):
                    dsig = "s"
                 elif(  dep <= 30):
                    dsig = "n"
                 elif(  dep <= 60):
                    dsig = "h"
                 elif(  dep <= 100):
                    dsig = "g"
                 else:
                    dsig = "a"


                 radius = mag*mag/40.0
                 print( "%.5f" % round(lon,5)," ",\
                        "%.5f" % round(lat,5)," ",\
                        "%.1f" % round(score,1)," ",\
                        "%.5f" % round(radius,5)," ",\
                        dsig, file=f)

                 # ....................=============================================
        else:    
            print("version: ",version)
        # ======================================================================
      else:
        print("version:  NULL")
        version = "000"

      i=i+1
      print("--------------------------------------",i)
  f.close()

if __name__ == "__main__":
    main()
